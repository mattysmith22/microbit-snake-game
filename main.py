from microbit import *;
import random;
from ninja_ide.dependencies.pep8mod import SELFTEST_REGEX

def Display(snake, apple):
    display.clear();
    for component in snake.sections:
        display.set_pixel(component.x, (4-component.y), 2)
    display.set_pixel(snake.head.x, (4-snake.head.y), 9);
    display.set_pixel(apple.x, (4-apple.y), 5);

class Coord(object): #a 2D Coordinate struct
    def __init__(self,x,y): #create a Coordinate
        self.x = x;
        self.y = y;

class Snake(object):
    def __init__(self, startx, starty): #initialising method
        self.head = Coord(startx, starty);
        self.sections = []; #the Coords for the sections
        self.countSections = 1; #the number of available sections
        self.sections.append(Coord(self.head.x, self.head.y));

    def move(self, rotation):
        #dy = delta y
        #dx = delta x
        if(rotation == 0): #decide on what vector to use
            dx = 0;
            dy = 1;
        elif(rotation == 1):
            dx = 1;
            dy = 0;
        elif(rotation == 2):
            dx = 0;
            dy = -1;
        elif(rotation == 3):
            dx = -1;
            dy = 0;
        
        self.head.x = self.head.x + dx;
        self.head.y = self.head.y + dy;
        
        finalCoord = correct(self.head)
        
        self.head.x = finalCoord.x;
        self.head.y = finalCoord.y;
        
        self.sections.append(finalCoord);
        
        while(len(self.sections) > self.countSections):
            print("Popping Old");
            self.sections.pop(0);
    
    def addPoint(self):
        self.countSections += 1;
    
    def hasLost(self):
        for i in self.sections[0:-1]:
            if(i.x == self.head.x and i.y == self.head.y):
                return True;
        return False;

def correct(CoordIn):
    x = CoordIn.x;
    y = CoordIn.y;
    
    if(x == 5):
        x = 0;
    elif(x == -1):
        x = 4;
    
    if(y == 5):
        y = 0;
    elif(y == -1):
        y = 4;
    
    return Coord(x, y);

def correctRotation(rotationIn):
    if(rotationIn == -1):
        rotation = 3;
    elif(rotationIn == 4):
        rotation = 0;
    else:
        rotation = rotationIn;
    return rotation;

def chooseAppleLocation(snake):
    while True:
        apple = Coord(random.randint(0, 4), random.randint(0, 4));
        if not any((i.x == apple.x and i.y == apple.y) for i in snake.sections):
            return apple;

def main():
    rotation = 0;
    snake = Snake(random.randint(0, 4), random.randint(0, 4));
    
    while True:
        apple = chooseAppleLocation(snake);
        
        while True:
            overallButtons = button_b.get_presses() - button_a.get_presses();
            button_a.reset_presses();
            button_b.reset_presses();
            
            if(overallButtons > 0):
                rotation = correctRotation(rotation + 1);
            elif(overallButtons < 0):
                rotation = correctRotation(rotation - 1);
            
            if(snake.hasLost()): #Checking if the snake has collided with itself
                print("End");
                return;
            if((snake.head.x == apple.x) and (snake.head.y == apple.y)): # Checking if the apple has collided with the snake
                print("Add Point");
                snake.addPoint();
                apple = chooseAppleLocation(snake);
                Display(snake, apple);
            
            snake.move(rotation);
            Display(snake, apple); 
            
            sleep(500);  
main();
display.show(Image.SAD);